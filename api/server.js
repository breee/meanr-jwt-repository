var express 				= require('express'),
		redis			      = require('./config/database'),
		JWTRedisSession = require('jwt-redis-session'),
		bodyParser		  = require('body-parser'),
		logger		 		  = require('morgan'),
		path		 		    = require('path'),
		config			    = require('./config/database'),
		User        		= require('./models/user'),
		mongoose		 		= require('mongoose'),
		jwt		 					= require('express-jwt'),
		authToken				= require('./config/auth'),
		app							= express(),
		jwtRouter				= express.Router();

//static path config
app.use(express.static(path.join(__dirname, '../app')))

//db config
mongoose.connect(config.database)

//logger config
app.use(logger('dev'))

//bodyparser config
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//JWT redis session config
app.use(JWTRedisSession({
	client	  : redis.client,
	secret	  : config.secretToken,
	keyspace  : "sess:",
	maxAge	  : 20,
	algorithm : "HS256",
	requestKey: "jwtSession",
	requestArg: "jwtToken"
}))

app.all('*', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', 'http://localhost');
	res.set('Access-Control-Allow-Credentials', true);
	res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
	res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, x-jwt-token');
	if ('OPTIONS' == req.method) return res.sendStatus(200);
	next();
});

//Routes
var routes = {};
routes.users = require('./routes/users.js')

app.post('/signup', function(req, res) {
	if (!req.body.username || !req.body.password) {
		res.json({success: false, msg: 'Please pass name and password.'});
	} else {
		var newUser = new User({
			username: req.body.username,
			password: req.body.password
		});
		// save the user
		newUser.save(function(err) {
			if (err) {
				return res.json({success: false, msg: 'Username already exists.'});
			}
			res.json({success: true, msg: 'Successful created new user.'});
		});
	}
});

jwtRouter.post('/authenticate', routes.users.login, jwt({secret: config.secretToken}))

//authed routes
jwtRouter.get('/', authToken.verifyToken, function(req, res) {
	res.json({success: true, message: "You got in!!!"})
});

app.use('/admin', jwtRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found')
	err.status = 404
	next(err)
})

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
		app.use(function(err, req, res, next) {
				res.status(err.status || 500);
				res.render('error', {
						message: err.message,
						error: err
				});
		});

		app.use(function (err, req, res, next) {
			if (err.name === 'UnauthorizedError') {
				res.send(401, 'invalid token...');
			}
		});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
				message: err.message,
				error: {}
		});
})

module.exports = app
