var config		= require('../config/database'),
		User			= require('../models/user');

module.exports.login = (req, res) => {
	var username = req.body.username || '';
	var password = req.body.password || '';

	if (username == '' || password == '') {
		return res.status(401).send({success: false, msg: "Undefined"});
	}

	User.findOne({
		username: req.body.username
	}, function(err, user) {
		if (err) {
			console.log(err);
			return res.send(401);
		}

		if (user == undefined || !user) {
			return res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
		} else {
			// check if password matches
			user.comparePassword(req.body.password, function (err, isMatch) {
				if (isMatch && !err) {
					// this will be stored in redis
					req.jwtSession.user = { username: user.username };

					// this will be attached to the JWT
					var claims = {
						iss: "my application name",
						aud: "myapplication.com",
						username: user.username
					};

					req.jwtSession.create(claims, function(error, token){
							return res.json({ token: token });
					})
				} else {
					console.log("Attempt failed to login with " + user.username);
					return res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
				}
			})
		}
	})
}

module.exports.logout = (req, res) => {

}
