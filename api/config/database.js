const redis 	= require('redis'),
	redisClient = redis.createClient(),
		 mongoose = require('mongoose');

redisClient.on("error", function (err) {
		console.log("Error " + err);
})
redisClient.on('connect', function () {
	console.log('Redis is ready...')
})

const mongodb = mongoose.connection;
mongodb.on('error', console.error.bind(console, 'connection error:'));
mongodb.once('open', function() {
	console.log("MongoDB is ready...")
});

module.exports = {
	secretToken : '137BAB574D',
	client			: redisClient,
	database		: 'mongodb://localhost/rest-socket'
}
