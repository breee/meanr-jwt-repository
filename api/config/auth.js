var User	 = require('../models/user'),
		config = require('./database'),
		jwt 	 = require('jsonwebtoken');

module.exports.verifyToken = (req, res, next) => {
	var token = req.headers['x-jwt-token']
	if (token) {
		// verifies secret and checks exp
		jwt.verify(token, config.secretToken, function(err, decoded) {
			if (err) {
				return res.status(401).send({ success: false, message: 'Failed to authenticate token.' });
				next();
			} else {
				// if everything is good, save to request for use in other routes
				User.findOne({
					username: decoded.username
				}, function(err, user) {
						if (err)
							return res.status(401).send({ success: false, message: 'Failed to authenticate token.' });
						if (!user) {
							return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
						} else {
							if(req.jwtSession.id) {
								console.log("Establish a verified a JWT session:" + req.jwtSession.claims.username + " is accessing the route with");
								console.log("Request JWT session data: ",
								req.jwtSession.id,
								req.jwtSession.claims,
								req.jwtSession.jwt
								);
							} else {
								return res.status(401).send({status: 'No session establish...'})
							}
							next();
						}
				});
			}
		});
	} else {
			if(token == null || token == "undefined" || token == ' ' || !token)
				return res.status(403).send({success: false, msg: 'No token provided.'});
	}
}
