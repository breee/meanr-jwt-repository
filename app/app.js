'use strict';

var app = angular.module('app', ['ngRoute', 'appControllers', 'appServices', 'appDirectives']);

var appServices = angular.module('appServices', []);
var appControllers = angular.module('appControllers', []);
var appDirectives = angular.module('appDirectives', []);

var options = {};
options.api = {};
options.api.base_url = "http://localhost:3000";


app.config(['$locationProvider', '$routeProvider',
function($location, $routeProvider) {
	$routeProvider.
	when('/admin/login', {
		templateUrl: 'partials/admin.signin.html',
		controller: 'AdminUserCtrl'
	}).
	when('/admin', {
		templateUrl: 'partials/admin.panel.html',
		controller: 'AdminPanel',
		access: { requiredAuthentication: true }
	}).
	when('/admin/logout', {
		templateUrl: 'partials/admin.logout.html',
		controller: 'AdminUserCtrl',
		access: { requiredAuthentication: true }
	}).
	otherwise({
		redirectTo: '/admin/login'
	});
}]);


app.config(function ($httpProvider) {
	$httpProvider.interceptors.push('TokenInterceptor');
});

app.run(function($rootScope, $location, $window, AuthenticationService) {
	$rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
		//redirect only if both isAuthenticated is false and no token is set
		if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
				&& !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {

			$location.path("/admin/login");
		}
	});
});

appControllers.controller('AdminUserCtrl', ['$scope', '$http', '$location', '$window', 'UserService', 'AuthenticationService',
function AdminUserCtrl($scope, $http, $location, $window, UserService, AuthenticationService) {

	//Admin User Controller (signIn, logOut)
	$scope.signIn = function signIn(username, password) {
		if (username != null && password != null) {

			UserService.signIn(username, password).success(function(data) {
				AuthenticationService.isAuthenticated = true;
				$window.sessionStorage.token = data.token;
				$location.path("/admin");
			}).error(function(status, data) {
				console.log(status);
				console.log(data);
			});
		}
	}

	$scope.logOut = function logOut() {
		if (AuthenticationService.isAuthenticated) {

			UserService.logOut().success(function(data) {
				AuthenticationService.isAuthenticated = false;
				delete $window.sessionStorage.token;
				$location.path("/admin/login");
			}).error(function(status, data) {
				console.log(status);
				console.log(data);
			});
		}
		else {
			$location.path("/admin/login");
		}
	}
}
]);


appControllers.controller('AdminPanel', ['$scope', '$http', function AdminPanel($scope, $http) {
	$http.get(options.api.base_url + '/admin').success(function(data) {
		console.log(data);
		}).error(function(status, data) {
			console.log(status);
			console.log(data);
		});
	}
]);

appDirectives.directive('displayMessage', function() {
	return {
		restrict: 'E',
		scope: {
			messageType: '=type',
			message: '=data'
		},
		template: '<div class="alert {{messageType}}">{{message}}</div>',
		link: function (scope, element, attributes) {
			scope.$watch(attributes, function (value) {
				console.log(attributes);
				console.log(value);
				console.log(element[0]);
				element[0].children.hide();
			});
		}
	}
});

appServices.factory('AuthenticationService', function() {
	var auth = {
		isAuthenticated: false,
		isAdmin: false
	}

	return auth;
});

appServices.factory('TokenInterceptor', function ($q, $window, $location, AuthenticationService) {
	return {
		request: function (config) {
//			config.headers = config.headers || {};
			if ($window.sessionStorage.token) {
				config.headers['x-jwt-token'] = $window.sessionStorage.token;
			}
			return config;
		},

		requestError: function(rejection) {
			return $q.reject(rejection);
		},

		/* Set Authentication.isAuthenticated to true if 200 received */
		response: function (response) {
			if (response != null && response.status == 200 && $window.sessionStorage.token && !AuthenticationService.isAuthenticated) {
				AuthenticationService.isAuthenticated = true;
			}
			return response || $q.when(response);
		},

		/* Revoke client authentication if 401 is received */
		responseError: function(rejection) {
			if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthenticationService.isAuthenticated)) {
				delete $window.sessionStorage.token;
				AuthenticationService.isAuthenticated = false;
				$location.path("/");
			}

			return $q.reject(rejection);
		}
	};
});

appServices.factory('UserService', ['$http', function($http){
	return {
		signIn: function(username, password) {
			return $http.post(options.api.base_url + '/admin/authenticate', {username: username, password: password});
		},

		logOut: function() {
			return $http.get(options.api.base_url + '/admin/logout');
		},

		register: function(username, password, passwordConfirmation) {
			return $http.post(options.api.base_url + '/user/register', {username: username, password: password, passwordConfirmation: passwordConfirmation });
		}
	}
}])
